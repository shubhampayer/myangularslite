import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  template: `
    <p>
      user-list works!
    </p>
    <a routerLink="/users/user-single.component">User Single Component</a>
  `,
  styles: [
  ]
})
export class UserListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
