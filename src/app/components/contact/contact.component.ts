import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  template: `
  <section class="hero is-primary is-bold">
    <div class="hero-body">
    <div class="container">
    <form class="my-4" (ngSubmit)="processForm()">
       <div class="form-group">
          <label for="name">Name</label>
          <input type="name" class="form-control" id="name" placeholder="Enter Your Nice Name" [(ngModel)]="name" required minlength="3" #nameInput="ngModel">
          <div class="help is-error" *ngIf="nameInput.invalid && nameInput.dirty">
              Name is required and needs to be at least 3 characters long.
          </div>
       </div>
        
       <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="Enter Your EmailID" [(ngModel)]="email" required email  #emailInput="ngModel">
       </div>

       <div class="form-group">
          <label for="message">Message</label>
          <input type="textarea" class="form-control" id="message" placeholder="What's in your Mind!" [(ngModel)]="message" required #messageInput="ngModel">
       </div>
      <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    </div>
    </div>
    </section>
`,
  styles: [
  ]
})
export class ContactComponent implements OnInit {
  name: string;
  email: string;
  message: string;
  constructor() { }

  ngOnInit(): void {
  }
  
  processForm() {
    const allInfo = `My name is ${this.name}. My email is ${this.email}. My message is ${this.message}`;
    alert(allInfo); 
  }
}
