import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
  <section class="hero is-info is-fullheight is-bold">
  <div class="hero-body">
  <div class="container">

    <h1 class="title">Home Page!</h1>

  </div>
  </div>
  </section>
  `,
  styles: [`
  .hero {
    background-image: url('assets/img/bg2.jpg') !important;
    background-size: cover;
    height: 450px;
    background-position: center center;
    background-repeat: no-repeat;
  }
  h1{
    color: white;
  }
   `]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
