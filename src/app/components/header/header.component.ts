import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
  <nav class="navbar bg-dark navbar-dark">
    <!-- logo -->
    <div class="navbar-brand">
       <a class="navbar-item">
         <img src="assets/img/angular-logo.png">
       </a>
    </div>
    <div class="navbar-menu">
      <div class="navbar-start d-flex">
        <a class="nav-link" routerLink="">Home</a>
        <a class="nav-link" routerLink="contact">Contact</a>
        <a class="nav-link" routerLink="users">Users</a>
     </div>
    </div>
  </nav>
`,
  styles: [`
  a{
    color: white;
  }
   `]
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
